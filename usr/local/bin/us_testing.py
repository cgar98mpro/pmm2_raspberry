import connection as conn
import signal
import time
from bluetooth import BluetoothServer
from sensor import UltraSoundSensor

######## CONSTANTS ########

# Sensor params
TRIG_PORT = 12 # Port 32
ECHO_PORT = 26 # Port 37

# Connection params
CONN_DELAY = 0.05 # Time in seconds
CONN_KO = 5 # Time in seconds

######### GLOBALS #########

global sens, server
global sensOn

########## FUNCS ##########

def onReceivedData(data):
    # Process msg
    global sensOn, sens
    (msgType, info) = conn.processSensorMsg(data)
    if msgType == conn.MSG_START:
        sensOn = True
    elif msgType == conn.MSG_END:
        sensOn = False
    elif msgType == conn.MSG_CONF:
        if info is not None:
            sens.setMaxDistance(info)
            server.send('D: {}cm\r\n'.format(info))

def onConnectionStart():
    print("App connection started")

def onConnectionEnd():
    print("App connection ended")

def deinit():
    global sens, server
    if server is not None:
        del server
    if sens is not None:
        del sens

def signalHandler(sig, frame):
    deinit()
    exit(1)

######### PROGRAM #########

# Modify KeyboardInterrupt
signal.signal(signal.SIGINT, signalHandler)

# Init peripherals
sens = UltraSoundSensor(TRIG_PORT, ECHO_PORT)

# Init status params
sensOn = False

# Init bluetooth server
server = BluetoothServer(onReceivedData,
                        onConnectionStart,
                        onConnectionEnd)
if server.isServerOn():
    
    # Server init worked
    print("BT Server OK")
    
    # Wait for user connection
    while not server.isClientConnected():
        pass
    
    # Manage user connection
    initTime = time.time()
    while server.isClientConnected():
        # Check journey state
        if sensOn:
            
            # Measure distance
            distance = sens.measure()
            
            # Do send info if ellapsed time >= CONN_DELAY
            if time.time() - initTime >= CONN_DELAY:
            
                # Notify measure start time
                server.send(time.strftime("%Y/%m/%d-%X") + "\r\n")
                
                # Notify distance
                server.send('Distance: {} cm\r\n'.format(distance))
                
                # Go next
                initTime = time.time()
    
    # Deinit variables
    deinit()
    
else:
    # Server init failed
    print("BT Server KO")
    deinit()
    time.sleep(CONN_KO)

# Finish program
exit(1)
