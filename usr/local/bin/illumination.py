import time
import neopixel

class LedStrip:
    
    # Pixel related params
    COLOR = [(0, 255, 0), (255, 255, 0), (255, 0, 0), (0, 0, 0)]
    DEFAULT_IDX = 3
    MAX_IDX = len(COLOR)
    
    # Illumination params
    TOP_BRIGHTNESS = 100
    BASE_BRIGHTNESS = [50, 20, 20, 0] # 0-100
    MAX_BRIGHTNESS = [50, TOP_BRIGHTNESS, TOP_BRIGHTNESS, 0]
    DIFF_BRIGHTNESS = [0, 10, 20, 0]
    TIME_BRIGHTNESS = [-1, 0.05, 0.01, -1]
    
    # Color modes
    COLOR_OK = 0
    COLOR_WARN = 1
    COLOR_DANGER = 2
    COLOR_OFF = 3
    
    def __init__(self, port, pixels, defaultIdx=DEFAULT_IDX):
        self.pixels = neopixel.NeoPixel(
            port,
            pixels,
            auto_write=False,
            pixel_order=neopixel.GRB
        )
        self.idx = -1
        self.setColorMode(defaultIdx)
    
    def __del__(self):
        if self.pixels != None:
            self.pixels.deinit()
    
    def setColorMode(self, colorMode):
        newIdx = colorMode % (self.MAX_IDX + 1)
        if newIdx != self.idx:
            self.idx = newIdx
            self.time = self.TIME_BRIGHTNESS[self.idx]
            self.brightness = self.BASE_BRIGHTNESS[self.idx]
            self.brightnessDir = True
            self.__applyUpdate()
    
    def update(self):
        if self.TIME_BRIGHTNESS[self.idx] > 0:
            if time.time() - self.initTime >= self.TIME_BRIGHTNESS[self.idx]:
                if self.brightnessDir == True: # Increase brightness
                    newBrightness = self.brightness + self.DIFF_BRIGHTNESS[self.idx]
                    if newBrightness >= self.MAX_BRIGHTNESS[self.idx]:
                        newBrightness = self.MAX_BRIGHTNESS[self.idx]
                        self.brightnessDir = False
                    self.brightness = newBrightness
                else: # Decrease brightness 
                    newBrightness = self.brightness - self.DIFF_BRIGHTNESS[self.idx]
                    if newBrightness <= self.BASE_BRIGHTNESS[self.idx]:
                        newBrightness = self.BASE_BRIGHTNESS[self.idx]
                        self.brightnessDir = True
                    self.brightness = newBrightness
                self.__applyUpdate()
        else:
            self.__applyUpdate()
    
    def __applyUpdate(self):
        self.pixels.brightness = self.brightness / self.TOP_BRIGHTNESS;
        self.pixels.fill(self.COLOR[self.idx])
        self.pixels.show()
        self.initTime = time.time()
