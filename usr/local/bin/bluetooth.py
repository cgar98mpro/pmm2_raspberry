from bluedot.btcomm import BluetoothServer as BTServer

class BluetoothServer:
    
    def __init__(self, onReceivedData, onConnectionStart, onConnectionEnd):
        try:
            self.server = BTServer(onReceivedData,
                                   when_client_connects=onConnectionStart,
                                   when_client_disconnects=onConnectionEnd)
        except:
            self.server = None
    
    def __del__(self):
        if self.server is not None:
            self.server.disconnect_client()
            self.server.stop()
    
    def isServerOn(self):
        return self.server is not None
    
    def isClientConnected(self):
        return False if self.server is None else self.server.client_connected
    
    def send(self, data):
        if self.server is not None:
            self.server.send(data)
