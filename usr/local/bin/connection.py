MSG_START = 1
MSG_END = 2
MSG_WARN = 3
MSG_CONF = 4
MSG_ERR = -1

HEAD_START = 'S'
HEAD_END = 'E'
HEAD_WARN = 'W'
HEAD_CONF = 'C'

def processMsg(msg):
    if msg[0] == HEAD_START:
        return (MSG_START, None)
    elif msg[0] == HEAD_WARN:
        info = None
        if len(msg) > 1:
            tmpInfo = msg[1:-2].split('/')
            if len(tmpInfo) == 2:
                tmpInfo = [str2num(tmpInfo[0]), str2num(tmpInfo[1])]
                if not (None in tmpInfo):
                    info = tmpInfo
        return (MSG_WARN, info)
    elif msg[0] == HEAD_END:
        return (MSG_END, None)
    else:
        return (MSG_ERR, None)

def processSensorMsg(msg):
    if msg[0] == HEAD_START:
        return (MSG_START, None)
    elif msg[0] == HEAD_CONF:
        info = None
        if len(msg) > 1:
            info = str2num(msg[1:-2])
        return (MSG_WARN, info)
    elif msg[0] == HEAD_END:
        return (MSG_END, None)
    else:
        return (MSG_ERR, None)

def str2num(string):
    if len(string) == 0:
        return None
    else:
        try:
            return float(string)
        except:
            return None

