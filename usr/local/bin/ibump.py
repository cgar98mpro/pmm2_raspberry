import board
import connection as conn
import signal
import time
from audio import Buzzer
from bluetooth import BluetoothServer
from illumination import LedStrip
from measureStats import MovingAverage
from sensor import UltraSoundSensor

######## CONSTANTS ########

# Sensor params
TRIG_PORT = 12 # Port 32
ECHO_PORT = 26 # Port 37

# Illumination params
ILLUM_PORT = board.D18   # Port 12
ILLUM_PIXELS = 10

# Audio params
AUDIO_PORT = 4 # Port 7
AUDIO_PERIOD = 0.1 # Period in seconds

# Connection params
CONN_DELAY = 0.05 # Time in seconds
CONN_KO = 5 # Time in seconds

# Distance statistics params
BUFF_SIZE = 8
DIST_DANGER = 20
DIST_WARN = 150

######### GLOBALS #########

global sens, buzzer, leds, server
global journeyOn, dist

########## FUNCS ##########

def onReceivedData(data):
    # Process msg
    global journeyOn, dist
    (msgType, info) = conn.processMsg(data)
    if msgType == conn.MSG_START:
        journeyOn = True
        server.send('S\r\n');
    elif msgType == conn.MSG_END:
        journeyOn = False
        server.send('E\r\n');
    elif msgType == conn.MSG_WARN:
        if info is not None:
            if info[0] > info[1]:
                info.reverse()
            dist = info
            server.send('W{}/{}\r\n'.format(dist[0], dist[1]))

def onConnectionStart():
    print("App connection started")

def onConnectionEnd():
    print("App connection ended")

def deinit():
    global sens, buzzer, leds, server
    if server is not None:
        del server
    if sens is not None:
        del sens
    if buzzer is not None:
        del buzzer
    if leds is not None:
        del leds

def signalHandler(sig, frame):
    deinit()
    exit(1)

######### PROGRAM #########

# Modify KeyboardInterrupt
signal.signal(signal.SIGINT, signalHandler)

# Init peripherals
sens = UltraSoundSensor(TRIG_PORT, ECHO_PORT)
buzzer = Buzzer(AUDIO_PORT, AUDIO_PERIOD)
leds = LedStrip(ILLUM_PORT, ILLUM_PIXELS)

# Init distance statistics
meas = MovingAverage(BUFF_SIZE)

# Init status params
journeyOn = False
dist = [DIST_DANGER, DIST_WARN]

# Init bluetooth server
server = BluetoothServer(onReceivedData,
                        onConnectionStart,
                        onConnectionEnd)
if server.isServerOn():
    
    # Server init worked
    print("BT Server OK")
    
    #while True:
    #    pass
    
    # Wait for user connection
    while not server.isClientConnected():
        pass
    
    # Manage user connection
    initTime = time.time()
    while server.isClientConnected():
        # Check journey state
        if journeyOn:
            
            # Measure distance
            distance = sens.measure()
            meas.newValue(distance)
            
            # Do send info if ellapsed time >= CONN_DELAY
            if time.time() - initTime >= CONN_DELAY:
            
                # Notify measure start time
                #server.send(time.strftime("%Y/%m/%d-%X") + "\r\n")
                
                # Notify average distance
                avgDist = meas.measureStat()
                avgDist = sens.maxDist if (avgDist is None) else avgDist
                server.send('D{:.2f}\r\n'.format(avgDist))
                
                # Manage average distance
                if avgDist < dist[0]:
                    buzzer.enable()
                    leds.setColorMode(LedStrip.COLOR_DANGER)
                else:
                    buzzer.disable()
                    if avgDist < dist[1]:
                        leds.setColorMode(LedStrip.COLOR_WARN)
                    else:
                        leds.setColorMode(LedStrip.COLOR_OK)
                initTime = time.time()
        
            # Manage LEDs & Buzzer
            leds.update()
            buzzer.update()
            
        else:
            leds.setColorMode(LedStrip.COLOR_OFF)
            buzzer.disable()
    
    # Deinit variables
    deinit()
    
else:
    # Server init failed
    print("BT Server KO")
    deinit()
    time.sleep(CONN_KO)

# Finish program
exit(1)
