import RPi.GPIO as GPIO
import time

class Buzzer:

    MIN_PERIOD = 0.001

    def __init__(self, port, period):
        self.port = port
        self.period = max(period, self.MIN_PERIOD);
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.port, GPIO.OUT)
        self.disable()
        
    def __del__(self):
        GPIO.cleanup([self.port])
    
    def enable(self):
        if self.enabled == False:
            GPIO.output(self.port, GPIO.LOW)
            self.enabled = True
            self.initTime = time.time()

    def disable(self):
        GPIO.output(self.port, GPIO.HIGH)
        self.enabled = False
    
    def update(self):
        if self.enabled == True:
            if time.time() - self.initTime >= (self.period / 2):
                GPIO.output(self.port, GPIO.LOW if GPIO.input(self.port) == 1 else GPIO.HIGH)
                self.initTime = time.time()
