import RPi.GPIO as GPIO
import time

class UltraSoundSensor:
    
    # Sensor constants
    TRIG_PULSE = 0.00001
    TIME_DIST_RATIO = 0.000058
    MAX_DIST = 400
    
    # Sensor echo modes
    ECHO_START = 1
    ECHO_END = 2
    
    def __init__(self, trigPort, echoPort, maxDist=MAX_DIST):
        
        # Get params
        self.trigPort = trigPort
        self.echoPort = echoPort
        self.maxDist = max(maxDist, 0)
        
        # Config GPIO
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.trigPort, GPIO.OUT)
        GPIO.setup(self.echoPort, GPIO.IN)
    
    def __del__(self):
        GPIO.cleanup([self.trigPort, self.echoPort])
    
    def measure(self):
        
        # Send 10us Trigger pulse
        GPIO.output(self.trigPort, True)
        time.sleep(self.TRIG_PULSE)
        GPIO.output(self.trigPort, False)
        
        # Wait for Echo pulse start
        if self.__waitEcho(self.ECHO_START) >= self.maxDist:
            return self.maxDist
        
        # Wait for Echo pulse end
        return self.__waitEcho(self.ECHO_END)
        
    def setMaxDistance(self, maxDist):
        self.maxDist = max(maxDist, 0)
    
    def __waitEcho(self, mode):
        
        # Wait for Echo pulse
        pulseCm = -1
        pulseStart = time.time()
        while pulseCm < 0 or GPIO.input(self.echoPort) == (False if mode == self.ECHO_START else True):
            
            # Check too long wait
            pulseEnd = time.time()
            pulseLength = pulseEnd - pulseStart
            pulseCm = pulseLength / self.TIME_DIST_RATIO
            if pulseCm >= self.maxDist:
                return self.maxDist
        
        # Return ellapsed time
        return pulseCm
