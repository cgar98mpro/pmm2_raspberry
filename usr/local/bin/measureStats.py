import statistics as stats

class MovingAverage:
    
    DEFAULT_SIZE = 3
    
    def __init__(self, size):
        self.size = size if size > 0 else self.DEFAULT_SIZE
        self.data = []
        
    def newValue(self, value):
        self.data.append(max(value, 0))
        if len(self.data) > self.size:
            self.data = self.data[(len(self.data) - self.size):]
    
    def measureStat(self):
        return stats.mean(self.data) if len(self.data) > 0 else None
