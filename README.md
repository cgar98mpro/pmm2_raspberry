# IBUMP INSTALL

# 1) Update Raspberry
sudo apt-get update -y
sudo apt-get upgrade -y

# 2) Install RPI.GPIO to use GPIO ports
sudo pip install RPi.GPIO -y

# 3) Install Bluedot library
sudo pip3 install bluedot -y
sudo pip3 install bluedot --upgrade -y

# 4) Install Neopixel libraries
sudo pip3 install --upgrade adafruit-python-shell
sudo wget https://raw.githubusercontent.com/adafruit/Raspberry-Pi-Installer-Scripts/master/raspi-blinka.py
sudo python3 raspi-blinka.py
sudo pip3 install rpi_ws281x adafruit-circuitpython-neopixel

# 4) Modify files to enable sudo python
mv ~/.bashrc ~/.bashrc.orig
sudo echo "export PYTHONPATH=/usr/bin/python" >> ~/.bashrc
mv /etc/sudoers /etc/sudoers.orig
sed -i "s/Defaults\tenv_reset/Defaults\tenv_reset\nDefaults\tenv_keep += \"PYTHONPATH\"/" /etc/sudoers

# 5) Move required files to Raspberry
cp -r etc/* /etc
cp -r usr/* /usr

# 6) Create 'ibump' service
sudo systemctl enable ibump.service


## EXTRA 1) Config Access Point

# Install library and stop services to start config
sudo apt-get install dnsmasq hostapd -y
sudo systemctl stop dnsmasq
sudo systemctl stop hostapd

# Config static IP
sudo cp /etc/dhcpcd.conf /etc/dhcpcd.conf.orig
sudo echo "interface wlan0\nstatic ip_address=192.168.1.8/24" >> /etc/dhcpcd.conf
sudo service dhcpcd restart

# Config DHCP server
sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
sudo cp etc/dnsmasq.conf /etc/dnsmasq.conf

# Config hostapd
sudo cp etc/hostapd/hostapd.conf /etc/hostapd/hostapd.conf
sudo cp /etc/default/hostapd /etc/default/hostapd.orig
sudo sed -i "s/DAEMON_CONF=.*/DAEMON_CONF=\"\/etc\/hostapd\/hostapd.conf\"/" /etc/default/hostapd

# Restart services
sudo service hostapd start
sudo service dnsmasq start

# Add routing and masquerade
sudo cp /etc/sysctl.conf /etc/sysctl.conf.orig
sudo sed -i "s/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/" /etc/sysctl.conf
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
sudo sh -c "iptables-save > /etc/iptables.ipv4.nat"
sudo cp /etc/rc.local /etc/rc.local.orig
sudo sed -i "s/exit 0/iptables-restore < \/etc\/iptables.ipv4.nat\nexit 0/" /etc/rc.local

# Reboot
sudo shutdown -r now


## EXTRA 2) Config ssh
sudo systemctl enable ssh
sudo systemctl start ssh
